module.exports = {
  // 项目名称
  title: '新佳宜超市',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 固定头部
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 是否显示菜单logo
  sidebarLogo: false,
};
