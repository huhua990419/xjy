// 导入设置token的方法
import { getToken, setToken, removeToken } from '@/utils/auth.js';
// 导入登录请求的方法
// import { login } from '@/api/user.js';
const state = {
  // 设置token的初始转态
  token: getToken(),
};

const mutations = {
  // 定义一个方法来修改token
  setToken(state, token) {
    // 这里修改的是vuex里面的token值
    state.token = token;
    // 调用方法来修改cookies里面的token
    setToken(token);
  },
  // 定义一个方法来删除token
  removeToken(state) {
    // 先清除state里面的token
    state.token = null;
    // 再清除cookies里面的token
    removeToken();
  },
};

const actions = {
  login(context, params) {
    // 调用登录接口
    // const res = await login(params);
    // if (res.data.status === 200) {
    //   // 调用登录接口成功,将token值存入vuex
    //   context.commit('setToken', res.data.token);
    // }
    // context.commit('setToken', res);
    context.commit('setToken', params);
  },
  // 定义一个实现退出登录的方法
  loginOut(context) {
    // 删除token
    context.commit('removeToken');
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
