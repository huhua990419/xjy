import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import app from './modules/app';
import settings from './modules/settings';
import user from './modules/user';

Vue.use(Vuex);

const store = new Vuex.Store({
  // 没有设置根级别的模块，设置了跟级别的getters,所有的数据都放在子模块里面
  modules: {
    app,
    settings,
    user,
  },
  // 快捷访问
  getters,
});

export default store;
