import axios from 'axios';
// 创建一个axios实例
const service = axios.create({
  baseURL: 'http://192.168.32.36:3000',
  timeout: 5000,
});
// 设置请求拦截器，用于注入token
service.interceptors.request.use();
// 设置响应拦截器，主要用于处理数据异常和数据结构异常问题
service.interceptors.response.use();
export default service;
