import store from '@/store/index.js';
import router from '@/router/index.js';
// import { getToken } from '@/utils/auth.js';

// 定义一个访问白名单
const whiteList = ['/login', '/404'];
router.beforeEach((to, from, next) => {
  if (store.getters.token) {
    if (to.path === '/login') {
      next('/');
    } else {
      // 直接放行
      next();
    }
  } else {
    if (whiteList.indexOf(to.path) > -1) {
      next();
    } else {
      next('/login');
    }
  }
});
