import request from '@/utils/request';

// 登录接口的封装
export function login(params) {
  return request({
    method: 'GET',
    url: '/login',
    params,
  });
}

// 注册
export function register(data) {
  return request({
    method: 'POST',
    url: '/register',
    data,
  });
}
