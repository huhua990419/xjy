import request from '@/utils/request';

export function getStore() {
  return request({
    method: 'GET',
    url: '/storediscount',
  });
}

export function addStore(data) {
  return request({
    method: 'POST',
    url: '/storediscount',
    data,
  });
}

export function delStore(id) {
  return request({
    method: 'DELETE',
    url: `/storediscount/${id}`,
  });
}

export function editStore(data) {
  return request({
    url: `/storediscount/${data.id}`,
    method: 'patch',
    data,
  });
}

export function Store(params) {
  return request({
    url: `/storediscount`,
    method: 'get',
    params,
  });
}
