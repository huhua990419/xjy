import request from '@/utils/request';
// 店铺行业
export function getStoreinformation(data) {
  return request({
    url: 'storeindustry',
    method: 'get',
    params: {
      _page: data.pagenum,
      _limit: data.pagesize,
    },
  });
}
// 店铺查询
export function queryindustry(params) {
  return request({
    url: 'storeindustry',
    method: 'get',
    params,

  });
}
// 店铺删除
export function shopdelete(id) {
  return request({
    url: `storeindustry/${id}`,
    method: 'delete',
  });
}
// 角色管理
export function rolemanagement() {
  return request({
    url: 'storeType',
    method: 'get',
  });
}

