import request from '@/utils/request';
// 获取所有数据
export function getShoplist1() {
  return request({
    method: 'GET',
    url: '/goods',
  });
}

// 分页查询
export function getShoplist2(params) {
  return request({
    method: 'GET',
    url: '/goods',
    params,
  });
}

// id查询
export function getShoplist3(id) {
  return request({
    method: 'GET',
    url: `/goods/${id}`,
  });
}

// 获取商品分类
export function getShoplist4() {
  return request({
    method: 'GET',
    url: '/goodsType',
  });
}

// 获取店铺名称
export function getShoplist5() {
  return request({
    method: 'GET',
    url: '/storeName',
  });
}

// 根据类型查询
export function getShoplist6(storetype) {
  return request({
    method: 'GET',
    url: `/goods/?storetype=${storetype}`,
  });
}

// 根据商品名查询
export function getShoplist7(storename) {
  return request({
    method: 'GET',
    url: `/goods/?storename=${storename}`,
  });
}

// 修改当前商品数
export function getShoplist8(data) {
  return request({
    method: 'put',
    url: `/goods/${data.id}`,
    data,
  });
}

// 删除当前商品
export function getShoplist9(id) {
  return request({
    method: 'DELETE',
    url: `/goods/${id}`,
  });
  //     url: `goods?id=${id}`
  //   })
}

// 修改数据
export function getShoplist10(data) {
  return request({
    method: 'put',
    url: `/goods/${data.id}`,
    data,
  });
}
// 根据商品名查询
export function getShoplist11(data) {
  return request({
    method: 'GET',
    url: `/goods/`,
    params: {
      storename: data.input,
      storetype: data.type,
      storeblone: data.store,

    },
  });
}
// 新增商品
export function increase(data) {
  return request({
    method: 'POST',
    url: `/goods`,
    data,
  });
}
