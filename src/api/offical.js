import request from '@/utils/request'

// 渲染列表 分页get
export function getOfficalList(data) {
  return request({
    url: '/material',
    method: 'get',
    params: {
      _page: data.page,
      _limit: data.size
    }
  })
}

// 删除delete
export function delOffical(id) {
  return request({
    url: `/material/${id}`,
    method: 'delete'
  })
}

// 新增post
export function addOffical(data) {
  return request({
    url: `/material`,
    method: 'post',
    data
  })
}

//编辑 put
export function editOffical(data) {
  return request({
    url: `/material/${data.id}`,
    method: 'put',
    data
  })
}

// 查询
export function iqeOffical(params) {
  return request({
    url: `/material`,
    method: 'get',
    params
  })
}

// 下拉行业
// 获取类型
export function getType() {
  return request({
    url: '/material',
    method: 'get'
  })
}
