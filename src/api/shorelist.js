import request from '@/utils/request'

export function shoreList(data) {
  return request({
    url: `/stores`,
    method: 'get',
    params: {
      _page: data.page,
      _limit: data.size
    }
  })
}

// 查询
export function iqeshore(params) {
  return request({
    url: '/stores',
    method: 'get',
    params
  })
}

// 下拉行业列表
export function getindustry() {
  return request({
    url: '/stores',
    method: 'get'
  })
}

// 删除
export function opendel(id) {
  return request({
    url: `/stores/${id}`,
    method: 'delete'
  })
}

// 新增
export function addshore(data) {
  return request({
    url: '/stores',
    method: 'post',
    data
  })
}

// 编辑
export function editshore(data) {
  return request({
    url: `/stores/${data.id}`,
    method: 'put',
    data
  })
}
