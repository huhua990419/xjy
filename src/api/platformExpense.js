import request from '@/utils/request';
export function platformExpense() {
  return request({
    method: 'get',
    url: 'http://192.168.32.29:3000/platformExpenseList',
  });
}
// xz
export function newplatformExpense(data) {
  return request({
    method: 'post',
    url: 'http://192.168.32.29:3000/platformExpenseList',
    data,
  });
}
// sc
export function terracedelete(id) {
  return request({
    url: `http://192.168.32.29:3000/platformExpenseList/${id}`,
    method: 'delete',
  });
}

export function withdraw() {
  return request({
    method: 'get',
    url: 'http://192.168.32.29:3000/platformIncomeList',
  });
}
