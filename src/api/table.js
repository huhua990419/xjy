import request from '@/utils/request';
//  获取分页接口
export function getauditpage(params) {
  return request({
    method: 'get',
    url: 'audit/',
    params,
  });
}
// 获取店铺行业
export function getaudit() {
  return request({
    method: 'get',
    url: 'audit/',
  });
}
// 查询店铺
export function getauditquery(params) {
  return request({
    method: 'get',
    url: 'audit/',
    params,
  });
}
// 订单列表
export function getOrder() {
  return request({
    method: 'get',
    url: '/orderlists',
  });
}
// 订单列表分页
export function getOrderPage(params) {
  return request({
    method: 'get',
    url: '/orderlists',
    params,
  });
}
// 删除订单
export function deleteOrderPage(id) {
  return request({
    method: 'delete',
    url: `/orderlists/${id}`,
  });
}
// 订单列表查询
export function GetOrderListQuery(params) {
  return request({
    method: 'GET',
    url: '/orderlists',
    params,
  });
}
// 修改编辑订单
export function EditOrderQuery(data) {
  return request({
    method: 'PATCH',
    url: `/orderlists/${data.id}`,
    data,
  });
}
// 添加订单
export function addOrderQuery(data) {
  return request({
    method: 'POST',
    url: `/orderlists`,
    data,
  });
}
