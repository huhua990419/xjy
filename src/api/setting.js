import request from '@/utils/request'

// 渲染列表
export function getsetting(data) {
  return request({
    url: '/systemset',
    method: 'get',
    params: {
      _page: data.page,
      _limit: data.size
    }
  })
}

// 查询店铺
export function iqesetting(params) {
  return request({
    url: `/systemset`,
    method: 'get',
    params
  })
}

// 删除
export function delsetting(id) {
  return request({
    url: `/systemset/${id}`,
    method: 'delete'
  })
}

// //新增
export function addsettin(data) {
  return request({
    url: '/systemset',
    method: 'post',
    data
  })
}

// 编辑
export function editsetting(data) {
  return request({
    url: `/systemset/${data.id}`,
    method: 'put',
    data
  })
}
