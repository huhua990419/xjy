import request from '@/utils/request';

export function getUniversal() {
  return request({
    method: 'GET',
    url: '/discount',
  });
}

export function addUniversal(data) {
  return request({
    method: 'POST',
    url: '/discount',
    data,
  });
}

export function delUniversal(id) {
  return request({
    method: 'DELETE',
    url: `/discount/${id}`,
  });
}

export function editUniversal(data) {
  return request({
    url: `/discount/${data.id}`,
    method: 'patch',
    data,
  });
}

export function Universal(params) {
  return request({
    url: `/discount`,
    method: 'get',
    params,
  });
}
