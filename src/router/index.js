import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/layout';

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true,
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '主页', icon: 'dashboard' },
      },
    ],
  },

  {
    path: '/example',
    component: Layout,
    redirect: '/example/storelist',
    name: 'Example',
    meta: { title: '店铺管理模块', icon: 'el-icon-takeaway-box' },
    children: [
      {
        path: 'storelist',
        name: 'Storelist',
        component: () => import('@/views/storelist/index'),
        // component: '@/views/table/index',
        meta: { title: '商铺列表', icon: 'el-icon-collection-tag' },
      },
      {
        path: 'shoplist',
        name: 'Shoplist',
        component: () => import('@/views/shoplist/index'),
        meta: { title: '商品列表', icon: 'el-icon-collection-tag' },
      },
      {
        path: 'openshop',
        name: 'Openshop',
        component: () => import('@/views/openshop/index'),
        meta: { title: '开店审核', icon: 'el-icon-collection-tag' },
      },
      {
        path: 'storesetting',
        name: 'storesetting',
        component: () => import('@/views/storesetting/index'),
        meta: { title: '店铺设置', icon: 'el-icon-collection-tag' },
      },
    ],
  },

  {
    path: '/shared',
    component: Layout,
    redirect: '/shared',
    children: [
      {
        path: 'shared',
        name: 'shared',
        component: () => import('@/views/shared/index'),
        meta: { title: '共享商圈管理', icon: 'el-icon-bangzhu' },
      },
    ],
  },

  {
    path: '/source',
    component: Layout,
    redirect: '/source/ordersreceived',
    name: 'Source',
    meta: { title: '订单管理模块', icon: 'el-icon-notebook-1' },
    children: [
      {
        path: 'ordersreceived',
        name: 'ordersreceived',
        component: () => import('@/views/ordersreceived/index'),
        meta: { title: '订单统计', icon: 'el-icon-notebook-2' },
      },
      {
        path: 'orderlist',
        name: 'orderlist',
        component: () => import('@/views/orderlist/index'),
        meta: { title: '订单列表', icon: 'el-icon-notebook-2' },
      },
    ],
  },

  {
    path: '/business',
    component: Layout,
    redirect: '/business/offical',
    name: 'Business',
    meta: { title: '素材管理模块', icon: 'el-icon-shopping-bag-1' },
    children: [
      {
        path: 'offical',
        name: 'offical',
        component: () => import('@/views/offical/index'),
        meta: { title: '官方素材', icon: 'el-icon-shopping-bag-2' },
      },
      {
        path: 'materiallist',
        name: 'materiallist',
        component: () => import('@/views/materiallist/index'),
        meta: { title: '素材列表', icon: 'el-icon-shopping-bag-2' },
      },
    ],
  },

  {
    path: '/discount',
    component: Layout,
    redirect: '/discount/universalcoupon',
    name: 'Discount',
    meta: { title: '优惠券管理模块', icon: 'el-icon-wallet' },
    children: [
      {
        path: 'universalcoupon',
        name: 'Universalcoupon',
        component: () => import('@/views/universalcoupon/index'),
        meta: { title: '通用优惠券管理', icon: 'el-icon-coin' },
      },
      {
        path: 'storecoupon',
        name: 'storecoupon',
        component: () => import('@/views/storecoupon/index'),
        meta: { title: '店铺优惠券管理', icon: 'el-icon-coin' },
      },
    ],
  },

  {
    path: '/finance',
    component: Layout,
    redirect: '/finance/platform',
    name: 'Finance',
    meta: { title: '财务管理模块', icon: 'el-icon-folder' },
    children: [
      {
        path: 'platform',
        name: 'platform',
        component: () => import('@/views/platform/index'),
        meta: { title: '平台收支明细', icon: 'el-icon-folder-add' },
      },
      {
        path: 'withdraw',
        name: 'withdraw',
        component: () => import('@/views/withdraw/index'),
        meta: { title: '提现管理', icon: 'el-icon-folder-add' },
      },
    ],
  },

  {
    path: '/system',
    component: Layout,
    redirect: '/system/setting',
    name: 'System',
    meta: { title: '系统设置模块', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'setting',
        name: 'setting',
        component: () => import('@/views/setting/index'),
        meta: { title: '设置', icon: 'el-icon-setting' },
      },
    ],
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true },
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes,
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
